#lang racket
(require racket/gui/base
         racket/cmdline
         png-image
         (prefix-in draw/ 2htdp/image)
         (prefix-in mrlib/ mrlib/image-core))
(define-struct viewport (scale x y))
(define tweak-canvas%
  (class canvas%
         (init-field keypress)
         (define/override (on-char e)
           (keypress e))
         (super-new)))
(define app% (class object%
                    (super-new)
                    (init-field [image (mrlib/bitmap->image image)])
                    (field [view '()])
                    (define/private (draw canvas dc)
                      (define v (if (empty? view) (initial-viewport image) view))
                      (define x (viewport-x v))
                      (define y (viewport-y v))
                      (send dc draw-text "Scale: " 10 10)
                      (mrlib/render-image (scale-to-viewport image v) dc x y))
                    (define/private (handle-keypress c)
                      ; (set-field! this ) ; CONTINUE
                      (displayln c))
                    (define window (new frame% [label "Tweak"]))
                    (define canvas
                      (new tweak-canvas% [parent window]
                                         [paint-callback (λ (c dc) (draw c dc))]
                                         [keypress (λ (c) (handle-keypress c))]))
                    (send window show #t)))

(define (initial-viewport image)
  (make-viewport 0.5 0 0))

(define (file->bitmap path)
  (let ([bitmap (make-bitmap 1 1)])
    (send bitmap load-file path)
    bitmap))

(define image-path (command-line
               #:program "tweak"
               #:usage-help "Usage: tweak <image>"
               #:args (image)
               image))

(define (scale-to-viewport bitmap view)
  (draw/scale (viewport-scale view) bitmap))

(unless (file-exists? image-path)
  (eprintf "No such file or directory: ~a\n" image-path)
  (exit 1))
(unless (png? image-path)
  (eprintf "Not a PNG: ~a\n" image-path)
  (exit 1))
(define app (new app% [image (file->bitmap image-path)]))

; (define window (new frame% [label "Tweak"]))
; (define tweak-canvas%
;   (class canvas%
;     (define/override (on-paint)
;       (let-values ([(width height) (send this get-gl-client-size)])
;         (printf "Painted: ~ax~a\n" width height)))
;     (super-new)))
; (new tweak-canvas% [parent window])
; (send window show #t)
